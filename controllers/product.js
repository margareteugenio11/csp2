const Product= require("../models/product")

module.exports.getAllActiveProducts =()=>{
	return Product.find({
		isActive: true
	})
}

module.exports.createProduct= (data) => {
	console.log(data)
	if(data.isAdmin){
		let newProduct = new Product({
			name: data.name,
			description: data.description,
			price: data.price
		});

		return newProduct.save().then((product, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		});
				
	}else{
		return false
		console.log(`You need Admin Access`)
	}
};

module.exports.updateProduct = (data) => {
	if(data.isAdmin){
		let updatedProduct ={
			name : data.name,
			description: data.description,
			price: data.price

		}
		return Product.findByIdAndUpdate(data.productId,updatedProduct).then((product,err)=>{
			return (err) ? false :true
		});
	}else{
		console.log(`You need Admin Access`)
	}
};

module.exports.archiveProduct = (data) => {
	if(data.isAdmin){
		let archivedProduct ={
			isActive: false

		}
		return Product.findByIdAndUpdate(data.productId,archivedProduct).then((product,err)=>{
			return (err) ? false :true
		});
	}else{
		console.log(`You need Admin Access`)
	}
};

module.exports.getSingleProduct =(productId)=>{
	return Product.findById({
		_id: productId
	})
};

