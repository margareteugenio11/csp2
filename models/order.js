const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({
	totalAmount : {
		type: Number,
		required : true
		
	},

	purchasedOn: {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String
		required: [true, `User ID is required`]
	},
	productId: {
		type: String
		required:[true, `Product ID is required`]
	}

})

module.exports = mongoose.model('order', orderSchema)
