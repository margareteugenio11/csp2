const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	email : {
		type: String,
		required: [true, 'Email field is required.']
	},

	password: {
		type: String,
		required: [true, 'Password field is required.']
	},

	isAdmin: {
		type: Boolean,
		default: false
	}
})

module.exports = mongoose.model('User', userSchema)
