const express = require("express");
const router = express.Router();
const userController = require("../controllers/user")
const auth = require("../auth")


// Register an account
router.post("/register", (req,res) => {
	userController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))

})

// Log in
router.post("/login", (req, res) => {
	userController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

//Get all user
router.get("/all", (req,res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController))
});

//For Specific User
router.get("/:userId", auth.verify, (req,res) => {
	userController.getUser(req.params).then(resultFromController => res.send(resultFromController))
})

// Admin Setup
router.put("/setadmin/:userId", auth.verify, (req, res) => {

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: req.params.userId
	}
	userController.setAdmin(data).then(resultFromSetAdmin =>res.send(resultFromSetAdmin))
})



module.exports = router
