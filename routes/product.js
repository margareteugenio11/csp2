const express = require("express");
const router = express.Router();
const productController = require("../controllers/product")
const auth = require("../auth")

router.get("/products",(req,res)=>{
	productController.getAllActiveProducts().then(resultFromGetAllActiveProducts => res.send(resultFromGetAllActiveProducts))
});

router.post("/createProduct", auth.verify,(req,res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		name: req.body.name,
		price: req.body.price,
		description: req.body.description
	}

	productController.createProduct(data).then(resultCreateProduct =>res.send(resultCreateProduct))
});

router.put("/updateProduct/:productID", auth.verify,(req,res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.params.productID,
		name: req.body.name,
		price: req.body.price,
		description: req.body.description
	}

	productController.updateProduct(data).then(resultUpdateProduct =>res.send(resultUpdateProduct))
});

router.put("/archiveProduct/:productID", auth.verify,(req,res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.params.productID,
	}

	productController.archiveProduct(data).then(resultArchiveProduct =>res.send(resultArchiveProduct))
});

router.get("/singleProduct/:productID",(req,res)=>{
	productController.getSingleProduct(req.params.productID)
	.then(resultFromGetSingleProduct => res.send(resultFromGetSingleProduct))

});

module.exports = router
