const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [true, 'Input Name.']
	},

	description: {
		type: String,
		required: [true, 'Product Description.']
	},

	price: {
		type: Number,
		
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type : Date, 
		default: new Date()
	}
})

module.exports = mongoose.model('product', productSchema)
