const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoute =require("./routes/user")
const productRoute =require("./routes/product")
const orderRoute =require("./routes/product")

const port = process.env.PORT || 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
app.use("/api",userRoute)
app.use("/pr",productRoute)
app.use("/dr",orderRoute)

mongoose.connect("mongodb+srv://admin:admin131@cluster0.ycdbp.mongodb.net/Capstone2?retryWrites=true&w=majority" , {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;
		
db.on('error', () => console.error.bind(console, "Connection Error"));
db.once('open', () => console.log('Connected to the cloud database.'))

app.listen(port, () => console.log (`Server running @ port: ${port}.`))