const express = require("express");
const router = express.Router();
const orderController = require("../controller/product")
const auth = require("../auth")

// Checkout
router.post("/createOrder", auth.verify,(req,res)=>{
	let data = {
		userID: req.body.userId,
		totalAmount: req.body.totalAmount
		productId: req.body.productId

	}
	orderController.createOrder(data).then(order=>res.send(order))
});

// Retrieve order 
router.get("/order",(req,res)=>{
	productController.getAllUserOrder().then(getAllUserOrder => res.send(getAllUserOrder))
});


module.exports = router

// Retrieve order (admin)
router.get("/order",(req,res)=>{
	productController.getAllAdminOrder().then(getAllAdminOrder => res.send(getAllAdminOrder))
});



router.post("/Order", auth.verify,(req,res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	}