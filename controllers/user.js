const User = require("../models/user")
const bcrypt = require("bcrypt")
const auth = require("../auth")


module.exports.checkEmailExists = (email) => {
	return User.find({email: email}).then(result => {
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
}

module.exports.register= (reqBody) => {
	return User.findOne({
		email: reqBody.email
	}) 
	.then((user)=>{
		if (user != null) {
			console.log(`${reqBody.email} Already Exist`)
		}else {
			let newUser = new User ({
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
			})

			return newUser.save().then((user, error) => {
				// console.log(user)
				if (error) {
					return false
				} else {
					return true
				}
			})
		}
	})
	
}


module.exports.login = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(user => {
		if (user == null) {
			return (`Account does not exist.`)
		} else {
			const isPwCorrect = bcrypt.compareSync(reqBody.password, user.password)
			if (isPwCorrect) {
				return {access: auth.createAccessToken(user)}
			} else {
				return (`Password is incorrect.`)
			}
		}
	})
}


module.exports.getAllUsers = (reqBody) => {
	return User.find({})
}

module.exports.getUser = (reqBody) => {
	return User.findById({_id: reqBody.userId}, {password: 0})
}

module.exports.setAdmin = (data) => {
	if(data.isAdmin){
		let updateAdmin ={
			isAdmin:true
		}
		return User.findByIdAndUpdate(data.userId,updateAdmin).then((user,err)=>{
			return (err) ? false :true
		})
	}else{
		console.log(`You need Admin Access`)
	}
	
}

